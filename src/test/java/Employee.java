import demo.EmployeeRequest;
import demo.EmployeeResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class Employee {

    @Test
    public void getEmployee() {
        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-type", "application/json")
                .header("Accept", "*/*")
                .get("/v1/employees");

//        response.getBody().prettyPrint();
//        System.out.println(response.getStatusCode());
        Assert.assertEquals(200, response.getStatusCode());

        Assert.assertThat("It takes too long...", response.getTime(), Matchers.lessThan(3000L));
        Assert.assertEquals("success", response.path("status"));

        EmployeeResponse employeeResponse = response.as(EmployeeResponse.class); // De-Serializer
        System.out.println(employeeResponse.getStatus());
        System.out.println(employeeResponse.getData().get(0).getEmployeeAge());
    }

    @Test
    public void createEmployee() {
//        String requestBody = "{\n" +
//                "  \"name\": \"Dana\",\n" +
//                "  \"salary\": \"123\",\n" +
//                "  \"age\": \"23\"\n" +
//                "}";

        EmployeeRequest employeeRequest = new EmployeeRequest(); // Serializer
        employeeRequest.setName("Bilal");
        employeeRequest.setAge("23");
        employeeRequest.setSalary("100000");

        Response response = RestAssured
                .given()
                .baseUri("http://dummy.restapiexample.com")
                .basePath("/api")
                .log()
                .all()
                .header("Content-type", "application/json")
                .header("Accept", "*/*")
//                .body(requestBody)
                .body(employeeRequest)
                .post("/v1/create");

        response.getBody().prettyPrint();
    }
}
